const breakpoints = {
  sm: 850,
  md: 1200,
};

export default breakpoints;
