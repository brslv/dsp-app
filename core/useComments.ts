import { useCallback, useState } from "react";
import IComment from "./types/IComment";
import * as API from "../lib/api";
import { toast } from "react-toastify";

type LoadComments = (arg0: {
  articleId?: number;
  parentCommentId?: number;
}) => Promise<IComment[]>;

type CommentData = {
  articleId: number;
  parentCommentId?: number;
  text: string;
};

export default function useComments() {
  const [loading, setLoading] = useState(false);
  const [comments, setComments] = useState<IComment[] | null>(null);

  const loadComments: LoadComments = useCallback(
    ({ articleId, parentCommentId }) => {
      setLoading(true);
      return new Promise((resolve) => {
        API.loadComments({ articleId, parentCommentId }).then((result) => {
          setComments(result.data);
          setLoading(false);
          resolve(result.data);
        });
      });
    },
    []
  );

  const addComment: (data: CommentData) => Promise<IComment> = useCallback(
    (data) => {
      return new Promise((resolve) =>
        API.addComment(data).then((result) => {
          toast.success("Comment added!", {
            draggable: true,
          });
          resolve(result.data);
        })
      );
    },
    []
  );

  return {
    loading,
    comments,
    loadComments,
    addComment,
  };
}
