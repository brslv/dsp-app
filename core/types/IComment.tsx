export default interface IComment {
  articleId: number;
  author: string;
  createdAt: number;
  id: number;
  parentCommentId: null | number;
  repliesCount: number;
  text: string;
}
