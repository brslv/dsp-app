export default interface IArticle {
  id: number;
  title: string;
  imageUrl: string;
  text: string;
  commentsCount: number;
  createdAt: number;
}
