import { useRouter } from "next/router";
import {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import * as API from "../lib/api";
import {
  calculateTotalPages,
  sanitizeArticleIdFromQuery,
  sanitizePageFromQuery,
} from "../lib/utils";
import IArticle from "./types/IArticle";

const ARTICLES_PER_PAGE = 5;

interface ICtx {
  articles: IArticle[] | null;
  selectedArticle: IArticle | null;
  loading: { init: boolean; more: boolean };
  showLoadMoreBtn: boolean;
  loadMore: () => void;
}

interface IProps {
  children: ReactNode;
}

const ArticlesContext = createContext<ICtx | null>(null);

export function ArticlesProvider({ children }: IProps) {
  const router = useRouter();
  const routerIsReady = router.isReady;
  const articleId = sanitizeArticleIdFromQuery(router.query.id as string);
  const page = sanitizePageFromQuery(router.query.page as string);

  const [selectedArticle, setSelectedArticle] = useState<IArticle | null>(null);
  const [totalPages, setTotalPages] = useState<number | null>(null);
  const [articles, setArticles] = useState<IArticle[] | null>(null);
  const [loading, setLoading] = useState({ init: false, more: false });

  const _requestArticles = (pagination: any) => {
    return new Promise((resolve) => {
      API.loadArticles(pagination).then((result) => {
        setTotalPages(
          calculateTotalPages(result.totalCount, ARTICLES_PER_PAGE)
        );
        setArticles((prev) => {
          if (Array.isArray(prev)) return [...prev, ...result.data];
          else return result.data;
        });
        resolve(result);
      });
    });
  };

  // Initial load - load all articles up to a given page.
  const initialLoad = useCallback(() => {
    if (articles || !routerIsReady) return;

    setLoading((prev) => ({ ...prev, init: true }));

    _requestArticles({
      offset: 0,
      limit: ARTICLES_PER_PAGE * page,
    }).then(() => setLoading((prev) => ({ ...prev, init: false })));
  }, [articles, page, routerIsReady]);

  const loadMore = () => {
    const query: { page: number; id?: number | null } = { page: page + 1 };
    if (router.query.id)
      query.id = sanitizeArticleIdFromQuery(router.query.id as string);

    router.push({ query });

    setLoading((prev) => ({ ...prev, more: true }));
    _requestArticles({
      offset: ARTICLES_PER_PAGE * page,
      limit: ARTICLES_PER_PAGE,
    }).then(() => setLoading((prev) => ({ ...prev, more: false })));
  };

  // Load the articles as soon as the provider is mounted.
  useEffect(() => initialLoad(), [initialLoad]);

  // Highlight selected article, based on URL query.id.
  useEffect(() => {
    if (articles && articleId !== null)
      setSelectedArticle(articles.find((art) => art.id === articleId) || null);
  }, [articles, articleId]);

  return (
    <ArticlesContext.Provider
      value={{
        articles,
        selectedArticle,
        loading,
        showLoadMoreBtn: !!(totalPages && page !== null && page < totalPages),
        loadMore,
      }}
    >
      {children}
    </ArticlesContext.Provider>
  );
}

export function useArticles() {
  const ctx = useContext(ArticlesContext);
  if (!ctx) throw new Error("Improper use of ArticlesProvider");
  return ctx;
}
