export default function Loader() {
  return (
    <div className="container">
      <div className="box">
        <div className="line" />
      </div>
      <style jsx>
        {`
          .box {
            width: 30px;
            height: 30px;
            border: 2px solid var(--color-loader-bg);
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 30px;
          }
          .line {
            width: 0;
            height: 2px;
            background: var(--color-loader-bg);
            animation: line 1s linear infinite;
          }
          @keyframes line {
            0% {
              width: 0;
            }
            50% {
              width: 30px;
            }
            100% {
              width: 0;
            }
          }
        `}
      </style>
    </div>
  );
}
