import { useEffect, useState } from "react";
import { useArticles } from "../../core/ArticlesProvider";
import useComments from "../../core/useComments";
import CommentForm from "../comments/CommentForm";
import Comment from "./Comment";
import Loader from "../Loader";
import usePrevious from "../../lib/usePrevious";
import IArticle from "../../core/types/IArticle";
import IComment from "../../core/types/IComment";

export default function CommentsList() {
  const { selectedArticle } = useArticles();
  const { loading, comments, loadComments, addComment } = useComments();

  // Whether the root comments have been loaded
  const [areCommentsInitialized, setAreCommentsInitialized] = useState(true);

  const prevSelectedArticle = usePrevious<IArticle | null>(selectedArticle);
  useEffect(() => {
    if (prevSelectedArticle?.id !== selectedArticle?.id) {
      setAreCommentsInitialized(false);
    }
  }, [prevSelectedArticle?.id, selectedArticle?.id]);

  // Load article's root comments on mount / selectedArticle change.
  useEffect(() => {
    loadComments({ articleId: selectedArticle!.id }).then(() =>
      setAreCommentsInitialized(true)
    );
  }, [loadComments, selectedArticle]);

  const onComment: (text: string) => Promise<IComment> = (text) => {
    return new Promise((resolve) => {
      addComment({ articleId: selectedArticle!.id, text }).then((comment) => {
        loadComments({ articleId: selectedArticle!.id }).then(() =>
          resolve(comment)
        );
      });
    });
  };

  return (
    <div>
      <h3>Comments</h3>

      <CommentForm onSubmit={onComment} />

      {loading && !areCommentsInitialized ? (
        <div className="loader-container">
          <Loader />
        </div>
      ) : null}

      {areCommentsInitialized && comments?.length
        ? comments?.map((comment) => (
            <Comment key={comment.id} data={comment} level={1} />
          ))
        : null}

      {areCommentsInitialized && !comments?.length ? (
        <div className="empty">Be the first to comment</div>
      ) : null}

      <style jsx>
        {`
          h3 {
            padding-bottom: var(--space);
          }
          .loader-container {
            padding-top: var(--space);
            display: flex;
            align-items: center;
            justify-content: center;
          }
          .empty {
            margin: var(--space) 0;
            background: var(--color-comment-bg);
            padding: var(--space);
            border-radius: var(--radius);
          }
        `}
      </style>
    </div>
  );
}
