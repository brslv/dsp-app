import IComment from "../../core/types/IComment";
import { useState } from "react";
import useComments from "../../core/useComments";
import cn from "classnames";
import CommentForm from "./CommentForm";
import Loader from "../Loader";
import { humanReadableDate } from "../../lib/utils";

interface IProps {
  data: IComment;
  level: number;
  onReply?: () => void;
}

export default function Comment({ data, level, onReply }: IProps) {
  const {
    comments: replies,
    loadComments,
    loading,
    addComment,
  } = useComments();
  const [areRepliesVisible, setAreRepliesVisible] = useState(false);
  const [isCommentFormActive, setIsCommentFormActive] = useState(false);

  // Keep track of new replies client-side, as there's no api to refetch
  // the whole comment with its replies.
  const [newlyAddedRepliesCount, setNewlyAddedRepliesCount] = useState(0);
  const repliesCount = data.repliesCount + newlyAddedRepliesCount;

  let repliesBtnLabel = `${areRepliesVisible ? "hide" : repliesCount} replies`;
  if (loading) repliesBtnLabel = "Just a sec...";

  const loadReplies = () => loadComments({ parentCommentId: data.id });

  const onRepliesCountClick = () => {
    if (repliesCount > 0) {
      if (!areRepliesVisible) loadReplies();
      setAreRepliesVisible(!areRepliesVisible);
    }
  };

  const onReplyClick = () => {
    setIsCommentFormActive(!isCommentFormActive);
  };

  const onReplySubmit: (text: string) => Promise<IComment> = (text) => {
    return new Promise((resolve) =>
      addComment({
        articleId: data.articleId,
        parentCommentId: data.id,
        text,
      }).then((reply) => {
        // Update the comments counter client-side
        setNewlyAddedRepliesCount(newlyAddedRepliesCount + 1);

        // Reload the replies from the api
        loadReplies().then(() => {
          setAreRepliesVisible(true);
          setIsCommentFormActive(false);
          onReply && onReply();
          resolve(reply);
        });
      })
    );
  };

  return (
    <div>
      <div className={cn("comment", "nested")}>
        <div className="meta">
          <div>
            <h5>{data.author}</h5>
            <time dateTime={new Date(data.createdAt).toISOString()}>
              {humanReadableDate(data.createdAt)}
            </time>
          </div>
          <div>
            <button
              onClick={onRepliesCountClick}
              className="btn"
              disabled={loading}
            >
              {repliesBtnLabel}
            </button>
            <button className="btn" onClick={onReplyClick} disabled={loading}>
              reply
            </button>
          </div>
        </div>

        <p>{data.text}</p>

        {isCommentFormActive ? (
          <div className="form-container">
            <CommentForm
              onSubmit={onReplySubmit}
              onCancel={() => setIsCommentFormActive(false)}
              autoFocus
            />
          </div>
        ) : null}
      </div>

      {areRepliesVisible && loading ? (
        <div className="loader">
          <Loader />
        </div>
      ) : null}

      {areRepliesVisible && !loading ? (
        <>
          {replies?.map((reply) => (
            <Comment
              key={reply.id}
              data={reply}
              level={level + 1}
              onReply={onReply}
            />
          ))}
        </>
      ) : null}

      <style jsx>
        {`
          .comment {
            margin: var(--space) 0;
            padding: var(--space);
            background: var(--color-comment-bg);
            border-radius: var(--radius);
          }
          .form-container {
            margin: calc(var(--space) * -1);
            margin-top: var(--space);
            background: var(--color-reply-form-bg);
            border-top: 1px solid var(--color-border);
            padding-top: var(--space);
            padding: var(--space);
            border-bottom-left-radius: var(--radius);
            border-bottom-right-radius: var(--radius);
          }
          .nested {
            margin-left: ${level === 1
              ? 0
              : `calc(var(--space) * ${level - 1})`};
          }
          .btn {
            text-decoration: underline;
            color: black;
          }
          .meta {
            display: flex;
            justify-content: space-between;
            padding-bottom: var(--space);
          }
          .meta div {
            display: flex;
            align-items: center;
          }
          .meta h5 {
            font-size: 1rem;
          }
          .meta time {
            padding-left: var(--space);
          }
          .meta button {
            margin: 0 var(--space);
            border: 0;
            padding: 0;
            background: transparent;
            text-decoration: underline;
            cursor: pointer;
          }
          .loader {
            display: flex;
            align-items: center;
            justify-content: center;
          }
        `}
      </style>
    </div>
  );
}
