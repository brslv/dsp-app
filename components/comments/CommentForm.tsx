import { ChangeEvent, FormEvent, useEffect, useRef, useState } from "react";
import { useArticles } from "../../core/ArticlesProvider";
import IComment from "../../core/types/IComment";
import Button from "../Button";

interface IProps {
  onSubmit: (text: string) => Promise<IComment>;
  onCancel?: () => void;
  autoFocus?: boolean;
}

export default function CommentForm({
  autoFocus = false,
  onSubmit,
  onCancel,
}: IProps) {
  const textareaRef = useRef<HTMLTextAreaElement>(null);
  const { selectedArticle } = useArticles();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [error, setError] = useState<string | null>(null);
  const [value, setValue] = useState("");

  useEffect(() => {
    if (autoFocus && textareaRef && textareaRef.current) {
      textareaRef.current.focus();
    }
  }, [autoFocus]);

  useEffect(() => reset(), [selectedArticle]);

  const reset = () => {
    setValue("");
    setError(null);
    setIsSubmitting(false);
  };

  const onChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setValue(e.target.value);
  };

  const _onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    setIsSubmitting(true);
    setError(null);

    if (!value) {
      setError("Nope! :)");
      setIsSubmitting(false);
      return;
    }

    onSubmit(value).then(reset);
  };

  return (
    <form onSubmit={_onSubmit}>
      <textarea
        ref={textareaRef}
        name="text"
        value={value}
        onChange={onChange}
        className={error ? "error" : undefined}
        disabled={isSubmitting}
      ></textarea>

      {error ? <p>{error}</p> : null}

      <div className="footer">
        <Button
          type="submit"
          disabled={isSubmitting}
          loading={isSubmitting}
          primary
        >
          Submit
        </Button>

        {onCancel ? (
          <div className="cancel">
            <Button
              type="button"
              onClick={onCancel}
              disabled={isSubmitting}
              secondary
            >
              Cancel
            </Button>
          </div>
        ) : null}
      </div>

      <style jsx>
        {`
          textarea {
            width: 100%;
            height: 60px;
            border: 2px solid var(--color-border);
            border-radius: var(--radius);
            resize: none;
            padding: var(--space);
          }

          textarea.error {
            border-color: red;
          }

          p {
            color: red;
            font-size: 0.8rem;
            margin: var(--space) 0;
            margin-top: 0;
          }

          .footer {
            display: flex;
            align-items: center;
          }

          .cancel {
            margin-left: calc(var(--space) / 2);
          }
        `}
      </style>
    </form>
  );
}
