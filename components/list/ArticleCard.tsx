import IArticle from "../../core/types/IArticle";
import cn from "classnames";
import Link from "next/link";
import Img from "../Img";

interface IProps {
  href: string | { pathname: string; query: { [key: string]: any } };
  imageUrl: string;
  title: IArticle["title"];
  text: IArticle["text"];
  commentsCount: IArticle["commentsCount"];
  isActive?: boolean;
}

export default function ArticleCard({
  href,
  imageUrl,
  title,
  text,
  commentsCount,
  isActive = false,
}: IProps) {
  return (
    <>
      <Link href={href} passHref>
        <article
          className={cn("article", {
            active: isActive,
          })}
        >
          <div className="img">
            <Img
              alt="Featured image"
              src={imageUrl}
              placeholder="blur"
              blurDataURL={imageUrl}
              layout="fill"
              objectFit="cover"
              objectPosition="center"
            />
          </div>

          <div className="main">
            <h2>{title}</h2>
            <p>{text}</p>
            <small>{commentsCount} comments</small>
          </div>
        </article>
      </Link>

      <style jsx>{`
        .article {
          display: flex;
          max-width: 100%;
          padding: var(--space);
          border-bottom: 1px solid var(--color-border);
          transition: background, var(--speed);
          cursor: pointer;
        }
        .article.active {
          background: var(--color-article-active);
        }
        .article:hover {
          background: var(--color-article-hover);
        }
        .article h2 {
          font-size: 1rem;
          font-weight: bold;
          padding-bottom: calc(var(--space) / 2);
        }
        .article .main {
          overflow: hidden;
          flex: 1;
        }
        .article .main p {
          overflow: hidden;
          text-overflow: ellipsis;
          white-space: nowrap;
        }
        .article small {
          color: var(--color-comments-count);
        }
        .article .img {
          overflow: hidden;
          border-radius: var(--radius);
          margin-right: var(--space);
          width: 100px;
          height: 100px;
          position: relative;
          border: 1px solid var(--color-border);
        }
      `}</style>
    </>
  );
}
