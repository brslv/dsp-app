import { useEffect } from "react";
import { useArticles } from "../../core/ArticlesProvider";
import Loader from "../Loader";
import ArticleCard from "./ArticleCard";
import { scroller, Element } from "react-scroll";
import { useRouter } from "next/router";
import { sanitizePageFromQuery } from "../../lib/utils";
import usePrevious from "../../lib/usePrevious";
import Button from "../Button";

export default function ArticlesList() {
  const router = useRouter();
  const { articles, selectedArticle, loading, showLoadMoreBtn, loadMore } =
    useArticles();
  const pageFromQuery = sanitizePageFromQuery(router.query.page as string);

  // Scroll to the selected article card after the initial load
  const prevLoadingInit = usePrevious(loading.init);
  useEffect(() => {
    if (prevLoadingInit && selectedArticle) {
      scroller.scrollTo(`article__${selectedArticle.id}`, {
        duration: 400,
        delay: 50,
        smooth: true,
        containerId: "articles-list",
        offset: -50,
      });
    }
  }, [selectedArticle, prevLoadingInit]);

  return (
    <div>
      {articles
        ? articles.map((article) => (
            <Element key={article.id} name={`article__${article.id}`}>
              <ArticleCard
                href={{
                  pathname: `/article/${article.id}`,
                  query: pageFromQuery ? { page: pageFromQuery } : {},
                }}
                imageUrl={article.imageUrl}
                title={article.title}
                text={article.text}
                commentsCount={article.commentsCount}
                isActive={article.id === selectedArticle?.id}
              />
            </Element>
          ))
        : null}

      {loading.init ? (
        <div className="loader">
          <Loader />
        </div>
      ) : null}

      {showLoadMoreBtn ? (
        <div className="btn-container">
          <Button
            primary
            loading={loading.more}
            disabled={loading.more}
            className="load-more-btn"
            onClick={loadMore}
          >
            Load more
          </Button>
        </div>
      ) : null}

      <style jsx>
        {`
          .loader {
            padding: var(--space);
          }
          .btn-container {
            display: flex;
            justify-content: center;
            padding: calc(var(--space) * 3) var(--space);
          }
        `}
      </style>
    </div>
  );
}
