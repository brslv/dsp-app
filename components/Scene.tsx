import breakpoints from "../core/breakpoints";
import { useArticles } from "../core/ArticlesProvider";
import ArticleDetail from "./detail/ArticleDetail";
import ArticlesList from "./list/ArticlesList";

export default function Scene() {
  const { selectedArticle } = useArticles();

  return (
    <section className="scene">
      <div className="container">
        <aside className="left" id="articles-list">
          <ArticlesList />
        </aside>

        <main className="right">
          <ArticleDetail />
        </main>
      </div>

      <style jsx>
        {`
          .scene {
            background: var(--color-scene-bg);
            max-height: 100vh;
            height: 100vh;
            padding: var(--scene-container-offset);
          }
          .container {
            overflow: hidden;
            border-radius: var(--radius);
            box-shadow: var(--scene-shadow);
            background: var(--color-scene-container-bg);
            display: grid;
            grid-template-columns: 3fr 6fr;
            height: 100%;
          }
          .left,
          .right {
            overflow: auto;
          }
          .left {
            background: var(--color-articles-list-bg);
            border-right: 1px solid var(--color-border);
            border-top-left-radius: var(--radius);
            border-bottom-left-radius: var(--radius);
          }

          @media screen and (max-width: ${breakpoints.md}px) {
            .container {
              grid-template-columns: 1fr 2fr;
            }
          }

          @media screen and (max-width: ${breakpoints.sm}px) {
            .container {
              grid-template-columns: 1fr;
              grid-template-rows: ${selectedArticle ? "1fr 3fr" : "1fr"};
            }

            .left {
              border-right: initial;
              border-bottom: 1px solid var(--color-border);
              border-top-left-radius: var(--radius);
              border-top-right-radius: var(--radius);
              border-bottom-left-radius: initial;
            }

            .right {
              display: ${!!selectedArticle ? "block" : "none"};
            }
          }
        `}
      </style>
    </section>
  );
}
