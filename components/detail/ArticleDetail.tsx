import { useArticles } from "../../core/ArticlesProvider";
import Img from "../Img";
import Loader from "../Loader";
import CommentsList from "../comments/CommentsList";

export default function ArticleDetail() {
  const { selectedArticle, loading } = useArticles();

  return (
    <div>
      <div className="detail">
        {loading.init ? (
          <div className="center">
            <Loader />
          </div>
        ) : null}

        {!selectedArticle && !loading.init ? (
          <div className="center">Select an article from the list</div>
        ) : null}

        {selectedArticle ? (
          <>
            <div className="article">
              <h1>{selectedArticle.title}</h1>
              <div className="img">
                <Img
                  src={selectedArticle.imageUrl}
                  alt="Featured image"
                  layout="fill"
                  objectFit="cover"
                  objectPosition="center"
                />
              </div>
              <p>{selectedArticle.text}</p>
            </div>
            <hr />

            <div className="comments">
              <CommentsList />
            </div>
          </>
        ) : null}
      </div>

      <style jsx>{`
        .detail {
          padding: calc(var(--space) * 2) var(--space);
          overflow: auto;
          max-width: 64ch;
          margin: auto;
          height: ${!selectedArticle ? "100vh" : "initial"};
          position: relative;
        }
        .detail h1 {
          padding-bottom: var(--space);
        }
        .detail .img {
          position: relative;
          width: 100%;
          height: 200px;
          border-radius: var(--radius);
          overflow: hidden;
          margin-bottom: var(--space);
        }
        .center {
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
        }
        .comments {
          padding-top: calc(var(--space) * 2);
        }
        hr {
          padding-top: calc(var(--space) * 2);
          border: 0;
          border-bottom: 1px solid var(--color-border);
        }
      `}</style>
    </div>
  );
}
