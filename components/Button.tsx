import { ComponentPropsWithoutRef } from "react";
import cn from "classnames";

interface IProps {
  loading?: boolean;
  primary?: boolean;
  secondary?: boolean;
}

export default function Button({
  loading = false,
  primary = false,
  secondary = false,
  className,
  ...rest
}: ComponentPropsWithoutRef<"button"> & IProps) {
  return (
    <>
      <button
        {...rest}
        className={cn("btn", { primary, secondary }, className)}
      >
        {loading ? "Just a sec..." : rest.children}
      </button>
      <style jsx>
        {`
          .btn {
            padding: calc(var(--space) / 2) var(--space);
            border: 0;
            border-radius: calc(var(--radius) / 2);
            cursor: pointer;
          }

          .btn.primary {
            background: var(--color-primary-btn-bg);
            color: var(--color-primary-btn-text);
          }

          .btn.secondary {
            background: var(--color-secondary-btn-bg);
            color: var(--color-secondary-btn-text);
          }
          .btn.secondary:hover {
            background: var(--color-secondary-btn-bg-hover);
          }

          .btn:hover {
            opacity: 0.5;
          }
        `}
      </style>
    </>
  );
}
