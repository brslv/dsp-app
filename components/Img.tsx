import React, { useEffect, useState } from "react";
import Image, { ImageProps } from "next/image";

const DEFAULT_IMG_SRC =
  "https://www.pplgroupllc.com/wp-content/uploads/2016/07/no-image-1.jpg";

export default function Img(props: ImageProps) {
  const [imgSrc, setImgSrc] = useState(props.src);

  useEffect(() => {
    setImgSrc(props.src);
  }, [props.src]);

  return (
    <Image
      alt="Image"
      {...props}
      src={imgSrc}
      onError={() => {
        setImgSrc(DEFAULT_IMG_SRC);
      }}
    />
  );
}
