import IArticle from "../../core/types/IArticle";
import IComment from "../../core/types/IComment";

type PaginationResult = {
  data: IArticle[];
  totalCount: number;
};

declare function loadArticles(paging?: {
  offset: number;
  limit: number;
}): Promise<PaginationResult>;

declare function loadComments({
  parentCommentId,
  articleId,
}: {
  parentCommentId?: number;
  articleId?: number;
}): Promise<{ data: IComment[] }>;

declare function addComment({
  articleId,
  parentCommentId,
  text,
}: {
  articleId?: number;
  parentCommentId?: number;
  text: string;
}): Promise<{ data: IComment }>;
