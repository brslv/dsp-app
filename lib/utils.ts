import day from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";

day.extend(relativeTime);

export const calculateTotalPages = (articlesCount: number, perPage: number) => {
  return Math.ceil(articlesCount / perPage);
};

export const sanitizePageFromQuery = (page?: string) => {
  const pageFromQuery = Number(page);
  return isNaN(pageFromQuery) ? 1 : pageFromQuery;
};

export const sanitizeArticleIdFromQuery = (id?: string) => {
  const articleId = Number(id);
  return isNaN(articleId) ? null : articleId;
};

export const humanReadableDate = (date: number) => {
  return day(date).fromNow();
};
